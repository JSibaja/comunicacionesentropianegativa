import numpy as np
import math
import random as rand

fuente = open("fuente.txt", "r")
sumidero = open("sumidero.txt", "w")

def promediarLista(lista):
    sum=0.0
    for i in range(0,len(lista)):
        sum=sum+lista[i]
 
    return sum/len(lista)

vT=[]
vR=[]
bkT=[]
bkR=[]
bfT=[]
bfR=[]
lista_temp = []
bcT=[]
bcR=[]
aux1=0
bc=[]
an = []
a_n = []
xT=[]
xR=[]
aux = []
yn = []
i = 1
b = 2   #se selecciona el b deseado segpun el orden de modulación que se quiere
NS = 0

# símb | bits  Síbolos del MD
#  a1  |  00
#  a2  |  01
#  a3  |  10
#  a4  |  11

#Se declaran las matrices para codificacion de canal
G= np.array([[1,0,0,0,1,0,1],
             [0,1,0,0,1,1,0],
             [0,0,1,0,0,1,1],
             [0,0,0,1,1,1,1]])

H= np.array([[1,1,0],
             [1,0,1],
             [0,1,1],
             [1,1,1],
             [1,0,0],
             [0,0,1],
             [0,1,0]])

R= np.array([[1,0,0,0],
            [0,1,0,0],
            [0,0,1,0],
            [0,0,0,1],
            [0,0,0,0],
            [0,0,0,0],
            [0,0,0,0]])

#CF
for line in fuente: #se recorren las líneas del documento
    for elemento in line:   #se recorren las elementos de las líneas
        vT.append(ord(elemento))    #se concatena el número decimal que corresponde con el caracter
        binn= "{0:b}".format(ord(elemento)) #se interpreta el número decimal como binario
        bkT.append(int(binn))
        x =int((7 - math.log(ord(elemento)+1,2))//1)
        y = range(x)
        if x > 0:
            for cero in y:
                bfT.append(0)
        for n in str(binn):
            bfT.append(int(n))

#CC
for num in range(0,(len(bfT)%4)): #Se agregan 0s para completar el bloque
    bfT.append(0)

for k in bfT:
    bc.append(k)    #se juntan de 4 en 4
    if len(bc) == 4:
        bcA=np.asarray(bc)  #se interpreta como array de numpy
        n=np.dot(bcA,G)     #se multiplica el vector por la matriz codificadora
        for bit in n:
            bcT.append(bit) #se concatena el resultado
        bc=[]   #se reinicia el vector donde se acumulan los 4 bits de entrada
bc=[]#se reinicia el vector nuevamente para su uso posterior

#se corrigen los numeros que no sean 0 o 1, ya que la multiplicación de matrices
#suma en base 10
for k in bcT:
    lista_temp.append(k%2)

bcT=lista_temp
lista_temp = []

#se define un NS mayor que 20 y multiplo de b

while NS < 20:
    NS = b * i
    i = i +1

i = 0

pk = []
#se define la funcion del pulso p(t) como secuencia de muestreos
for elemento in range(0,NS):
    pk.append(1)

#MD
#se completa la secuencia de con ceros
for num in range(0,(len(bcT)%b)):
    bcT.append(0)
    i=i+1

simbolos = []
#se realiza la asignacion a simbolos segun b
for num in range(0,2**b):
    simbolos.append([])
    for x in range(0,b-len([int(i) for i in bin(num)[2:]])):
        
        simbolos[num].append(0) 
    simbolos[num].extend([int(i) for i in bin(num)[2:]])   

#se agrupan los bits para crear una secuencia de simbolos segun la asignacion anterior
for k in bcT:
    bc.append(k)   
             #los junto de 2 en 2
    if len(bc) == b:
        i = 0
        for simbolo in np.arange(-1, 1+2/(2**b), 2/(2**b)):
        #asignación de símbolos
            if simbolo != 0:
                if bc == simbolos[i]:
                    an.append(simbolo)
                i = i+1
        bc=[] 

#se multiplica cada simbolo por el pulso y se concatena en una secuencia 
#que representa el muestreo de la señal
for n in range(0,len(an)):
    for m in range(0,len(pk)):
        xT.append(an[n]*pk[m])



#Canal con ruido blanco aditivo Gaussiano
mean = 0
std = 0.1 
num_samples = len(xT)
ruido = np.random.normal(mean, std, size=num_samples)
i=0
for k in xT:
    xR.append(k+ ruido[i])
    i = i + 1

#dMD
#se agrupan las muestras en NS simbolos y se calcula el promedio
for k in xR:
    aux.append(k)   
             #los junto de 2 en 2
    if len(aux) == NS:
        yn.append(promediarLista(aux))
        aux=[] 

min = 2
ind = 0

#se mapean los promedios obtenidos a los simbolos
for k in yn:
    for simbolo in np.arange(-1, 1+2/(2**b), 2/(2**b)):
        if np.abs(k-simbolo) < min:
            min = np.abs(k-simbolo)
            ind = simbolo
        
        
    min = 2
    a_n.append(ind)

#se cambian los simbolos a las secuencias de bits
for k in a_n:
    i=0
    for simbolo in np.arange(-1, 1+2/(2**b), 2/(2**b)):
        if simbolo != 0:
            if simbolo == k:
                
                bcR.extend(simbolos[i])
            i=i+1

h=0
#dCC
for bit in bcR:
    bc.append(bit)
    if len(bc) == 7:
        bcA=np.asarray(bc)
        S=np.dot(bcA,H) #se multiplica el vector por la matriz H
        #se corrigen los numeros que no sean 0 o 1, ya que la 
        #multiplicación de matrices suma en base 10
        for k in S:
            S[h]=k%2
            h=h+1
        h=1
        s=0
        for fila in H:
            if (np.array_equal(S,fila)): #se compara el sindrome con la matriz H
                s=h
            h=h+1
        h=0
        if(s != 0):
            bcA[s-1]=(bcA[s-1]+1)%2 #se corrige un bit del vector, segun el sindrome
        n=np.dot(bcA,R) #se multiplica el vector por la matriz decodificadora     
        for dato in n:          
            bfR.append(dato)    
            bc=[]                   

i=0
#dCF
for k in bfR:
    aux1 = aux1*2 + k 
    i = i + 1
    if i == 7:
        bkR.append(aux1)
        vR.append(chr(aux1))
        sumidero.write(chr(aux1))
        aux1=0
        i=0

fuente.close()
sumidero.close()