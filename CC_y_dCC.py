import numpy as np
import math

bfR=[]
lista_temp = []
bcT=[]
bcR=[]
aux=0
i = 0
bc=[]

G= np.array([[1,0,1,0,0,1,0],
             [1,0,0,1,1,0,0],
             [0,1,0,1,0,1,0],
             [1,0,0,1,0,1,1]])

H= np.array([[1,0,0],
             [0,1,1],
             [1,1,0],
             [0,0,1],
             [1,0,1],
             [0,1,0],
             [1,1,1]])

R= np.array([[0,0,0,0],
             [0,0,1,0],
             [1,0,0,0],
             [0,0,0,0],
             [0,1,0,0],
             [0,0,0,0],
             [0,0,0,1]])

#cadena de prueba para el CC
bfT=[1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,
     1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,
     1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,
     1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,
     1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,]
#CC
for num in range(0,(len(bfT)%4)):
    bfT.append(0)

for k in bfT:
    bc.append(k)                #los junto de 4 en 4
    if len(bc) == 4:
        bcA=np.asarray(bc)      #lo interpreto como array de numpy
        n=np.dot(bcA,G)         #multiplico el vector de 4 por la matriz codificadora
              
        for bit in n:
            bcT.append(bit)     #concateno el resultado
            
        bc=[]                             
                        
bc=[]

#esto es para corregir los numeros que no sean 0 o 1, y que la multiplicación de matrices suma en base 10
for k in bcT:
    lista_temp.append(k%2)

bcT=lista_temp
lista_temp = []

#canal ideal
bcR=bcT
h=0

for bit in bcR:
    bc.append(bit)                
    if len(bc) == 7:
        bcA=np.asarray(bc)      #se interpreta como array de numpy
        S=np.dot(bcA,H)
        for k in S:
            S[h]=k%2
            h=h+1
        h=1

        s=0
        for fila in H:
            if (np.array_equal(S,fila)):
                s=h
            h=h+1
        h=0
        if(s != 0):
            bcA[s-1]=(bcA[s-1]+1)%2
        n=np.dot(bcA,R)         #multiplico el vector de 4 por la matriz codificadora
        for dato in n:          
            bfR.append(dato)    #concateno el resultado
        bc=[]                   #reinicio el vector donde acumulo los 4 bits de entrada

print(bfR)