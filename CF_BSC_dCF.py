import numpy as np
import math
import random as rand

fuente = open("fuente.txt", "r")
sumidero = open("sumidero.txt", "w")

vT=[]
vR=[]
bkT=[]
bkR=[]
bfT=[]
bfR=[]
aux=0
i = 0

for line in fuente:
    for elemento in line:
        vT.append(ord(elemento))
        bin= "{0:b}".format(ord(elemento))
        bkT.append(int(bin))
        x =int((7 - math.log(ord(elemento)+1,2))//1)
        y = range(x)
        if x > 0:
            for cero in y:
                bfT.append(0)
        for n in str(bin):
            bfT.append(int(n))

p=0.01              #probabilidad
for bit in bfT:
    v=rand.random() #obtenemos un random entre 0 y 1
    if v < p:
        b=(bit+1)%2 #obtenemos el bit opuesto
        bfR.append(b)
    else:
        bfR.append(bit)

for k in bfR:
    aux = aux*2 + k 
    i = i + 1
    if i == 7:
        bkR.append(aux)
        vR.append(chr(aux))
        sumidero.write(chr(aux))
        aux=0
        i=0

fuente.close()
sumidero.close()